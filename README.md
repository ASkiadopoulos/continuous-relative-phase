# Continuous relative phase

## Description
The continuous relative phase between two sinusoidal oscillators, SA and SB, is calculated through the following steps:

Firstly, the signals are centered around zero. Subsequently, they undergo Hilbert transformation to generate analytic signals, denoted as HA and HB. The arctangent of the product of HA with the conjugate of HB is then computed.

Positive CRP values indicate that SB leads SA in phase space.

## License
MIT License
